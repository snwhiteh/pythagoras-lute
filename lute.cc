#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include "shader_utils.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define PI 3.141592653587
#define OCTAGON

int screen_width = 1600, screen_height = 900;


#ifdef HEXAGON
const float r = 0.5;
const int strings = 6; 
const float c = 0.86602540378;
const int dim = 2;
const float basic_vertices[] = {
    1.0, 0.0, cosf(PI/3), sinf(PI/3),
    cosf(PI/3), sinf(PI/3), cosf(2*PI/3), sinf(2*PI/3),
    cosf(2*PI/3), sinf(2*PI/3), cosf(3*PI/3), sinf(3*PI/3),
    cosf(3*PI/3), sinf(3*PI/3), cosf(4*PI/3), sinf(4*PI/3),
    cosf(4*PI/3), sinf(4*PI/3), cosf(5*PI/3), sinf(5*PI/3),
    cosf(5*PI/3), sinf(5*PI/3), 1.0, 0.0,
    1.0, 0.0, cosf(3*PI/3), sinf(3*PI/3),
    cosf(PI/3), sinf(PI/3), cosf(4*PI/3), sinf(4*PI/3),
    cosf(2*PI/3), sinf(2*PI/3), cosf(5*PI/3), sinf(5*PI/3)
};
const float v[] = {
    cosf(PI/6), sinf(PI/6),
    cosf(3*PI/6), sinf(3*PI/6),
    cosf(5*PI/6), sinf(5*PI/6),
    cosf(7*PI/6), sinf(7*PI/6),
    cosf(9*PI/6), sinf(9*PI/6),
    cosf(11*PI/6), sinf(11*PI/6),
};
const int edges_per_strata = sizeof(basic_vertices)/(4*sizeof(float));
#endif

#ifdef PENTAGON
const float r = 0.61803398874989;
const int strings = 1;
const float c = 1;
const int dim = 2;
const float basic_vertices[] = {
    1.0, 0.0, cosf(2*PI/5), sinf(2*PI/5),
    cosf(4*PI/5), sinf(4*PI/5), cosf(2*PI/5), sinf(2*PI/5),
    cosf(4*PI/5), sinf(4*PI/5), cosf(6*PI/5), sinf(6*PI/5),
    cosf(8*PI/5), sinf(8*PI/5), cosf(6*PI/5), sinf(6*PI/5),
    cosf(8*PI/5), sinf(8*PI/5), cosf(10*PI/5), sinf(10*PI/5),

    1.0, 0.0, cosf(4*PI/5), sinf(4*PI/5),
    cosf(4*PI/5), sinf(4*PI/5), cosf(8*PI/5), sinf(8*PI/5),
    cosf(2*PI/5), sinf(2*PI/5), cosf(8*PI/5), sinf(8*PI/5),
    cosf(2*PI/5), sinf(2*PI/5), cosf(6*PI/5), sinf(6*PI/5),
    cosf(10*PI/5), sinf(10*PI/5), cosf(6*PI/5), sinf(6*PI/5),
};

const float v[] = {
    cosf(PI/5), sinf(PI/5),
    cosf(3*PI/5), sinf(3*PI/5),
    cosf(5*PI/5), sinf(5*PI/5),
    cosf(7*PI/5), sinf(7*PI/5),
    cosf(9*PI/5), sinf(9*PI/5),
    cosf(2*PI/5), sinf(2*PI/5),
    cosf(4*PI/5), sinf(4*PI/5),
    cosf(6*PI/5), sinf(6*PI/5),
    cosf(8*PI/5), sinf(8*PI/5),
    cosf(10*PI/5), sinf(10*PI/5),
};
const int edges_per_strata = sizeof(basic_vertices)/(4*sizeof(float));
#endif

#ifdef OCTAGON
const float r = 0.41421356237;
const int strings = 4; 
const float c = 0.76536686;
const int dim = 2;
const float basic_vertices[] = {
    1.0, 0.0, cosf(PI/4), sinf(PI/4),
    cosf(PI/4), sinf(PI/4), cosf(2*PI/4), sinf(2*PI/4),
    cosf(2*PI/4), sinf(2*PI/4), cosf(3*PI/4), sinf(3*PI/4),
    cosf(3*PI/4), sinf(3*PI/4), cosf(4*PI/4), sinf(4*PI/4),
    cosf(4*PI/4), sinf(4*PI/4), cosf(5*PI/4), sinf(5*PI/4),
    cosf(5*PI/4), sinf(5*PI/4), cosf(6*PI/4), sinf(6*PI/4),
    cosf(6*PI/4), sinf(6*PI/4), cosf(7*PI/4), sinf(7*PI/4),
    cosf(7*PI/4), sinf(7*PI/4), 1.0, 0.0,
    1.0, 0.0, cosf(3*PI/4), sinf(3*PI/4),
    cosf(PI/4), sinf(PI/4), cosf(4*PI/4), sinf(4*PI/4),
    cosf(2*PI/4), sinf(2*PI/4), cosf(5*PI/4), sinf(5*PI/4),
    cosf(3*PI/4), sinf(3*PI/4), cosf(6*PI/4), sinf(6*PI/4),
    cosf(4*PI/4), sinf(4*PI/4), cosf(7*PI/4), sinf(7*PI/4),
    cosf(5*PI/4), sinf(5*PI/4), cosf(8*PI/4), sinf(8*PI/4),
    cosf(6*PI/4), sinf(6*PI/4), cosf(1*PI/4), sinf(1*PI/4),
    cosf(7*PI/4), sinf(7*PI/4), cosf(2*PI/4), sinf(2*PI/4),
 /*    1.0, 0.0, cosf(4*PI/8), sinf(4*PI/8),
    cosf(PI/4), sinf(PI/4), cosf(5*PI/4), sinf(5*PI/4),
    cosf(2*PI/4), sinf(2*PI/4), cosf(6*PI/4), sinf(6*PI/4),
    cosf(3*PI/4), sinf(3*PI/4), cosf(7*PI/4), sinf(7*PI/4)*/
};
const float v[] = {
    cosf(PI/8), sinf(PI/8),
    cosf(5*PI/8), sinf(5*PI/8),
    cosf(9*PI/8), sinf(9*PI/8),
    cosf(13*PI/8), sinf(13*PI/8),
};
const int edges_per_strata = sizeof(basic_vertices)/(4*sizeof(float));
#endif

#ifdef DODECAHEDRON
const int edges_per_strata = 60;
const float r = 0.61803398874989;
const float tau = 1+r;
const float c = 1.0/(2.0+tau+tau+r);
const int dim = 3;
const float vertices[3*20] {
    1.0, 1.0, 1.0,
    1.0, 1.0, -1.0,
    1.0, -1.0, 1.0,
    1.0, -1.0, -1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0, -1.0,
    -1.0, -1.0, 1.0,
    -1.0, -1.0, -1.0,
    0, tau, r,
    0, tau, -r,
    0, -tau, r,
    0, -tau, -r,
    r, 0, tau,
    r, 0, -tau,
    -r, 0, tau,
    -r, 0, -tau,
    tau, r, 0,
    tau, -r, 0,
    -tau, r, 0,
    -tau, -r, 0
};

// to be filled out in init_resources
float basic_vertices[3*2*60] = {
};

const int strings = 12;
const float v[] {
    2.0 + tau + tau + r, 0.0, 2.0 + tau,
    -(2.0 + tau + tau + r), 0.0, 2.0 + tau,
    -(2.0 + tau + tau + r), 0.0, -(2.0 + tau),
    2.0 + tau + tau + r, 0.0, -(2.0 + tau),
    2.0 + tau,2.0 + tau + tau + r,  0.0,
    2.0 + tau,-(2.0 + tau + tau + r),  0.0,
     -(2.0 + tau),-(2.0 + tau + tau + r), 0.0,
    -(2.0 + tau),2.0 + tau + tau + r,  0.0,
    0.0,2.0 + tau,2.0 + tau + tau + r,
    0.0,2.0 + tau,-(2.0 + tau + tau + r), 
    0.0,-(2.0 + tau),-(2.0 + tau + tau + r),
    0.0,-(2.0 + tau),2.0 + tau + tau + r,
};
#endif

#ifdef DODECAPLEX
#include "dodecaplex_data.h"
#endif

const int depth = 100;
const int first_layer = -2;
const int elts_per_strata = 2 * edges_per_strata * strings;

GLuint vao;
GLuint program;
GLint attribute_coord4d, attribute_v_color;
GLuint vbo_lute;
GLint uniform_fade;
GLint uniform_mvp;

struct attributes {
    GLfloat coord4d[4];
    GLfloat v_color[3];
};

bool init_resources() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

#ifdef DODECAHEDRON
    int edges_used = 0;
    for (int i = 0; i < 20; ++i) {
        float x1 = vertices[3*i];
        float y1 = vertices[3*i+1];
        float z1 = vertices[3*i+2];
        for (int j = i+1; j < 20; ++j) {
            float x2 = vertices[3*j];
            float y2 = vertices[3*j+1];
            float z2 = vertices[3*j+2];
            float norm = sqrtf((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
            if (abs(norm-sqrt(5.0)+1) < 0.01 || abs(norm-sqrt(5.0)-1.0) < 0.01) {
                basic_vertices[6*edges_used] = x1;
                basic_vertices[6*edges_used+1] = y1;
                basic_vertices[6*edges_used+2] = z1;
                basic_vertices[6*edges_used+3] = x2;
                basic_vertices[6*edges_used+4] = y2;
                basic_vertices[6*edges_used+5] = z2;
                ++edges_used;
            }
        }
    }
#endif
#ifdef DODECAPLEX
    populate_vertices();
#endif

    cerr << "Here" << endl;
    struct attributes lute_attributes[depth * elts_per_strata];

    for (int i = 0; i < depth; ++i) {
        for (int k = 0; k < strings; ++k) { 
            float vx = c*v[dim*k], vy = c*v[dim*k+1];
            float vz = (dim > 2 ? c*v[dim*k+2] : 0.0);
            float vt = (dim > 3 ? c*v[dim*k+3] : 0.0);
            for (int j = 0; j < edges_per_strata; ++j) {
                auto f = [i,j](float x,float v) -> float { return pow(r, i+first_layer)*x - pow(r, i+first_layer)/(1.0-r)*v; };
                auto colx = [](float x) -> float { return fminf(1.0, fmaxf(0.0, -log(abs(x))/12.0)); };
                float wx1 = f(basic_vertices[2*dim*j], vx);
                float wy1 = f(basic_vertices[2*dim*j+1],vy);
                float wz1 = (dim > 2 ? f(basic_vertices[2*dim*j+2],vz) : 0.0);
                float wt1 = (dim > 3 ? f(basic_vertices[2*dim*j+3],vt) : 0.0);
                float wx2 = f(basic_vertices[2*dim*j+dim], vx);
                float wy2 = f(basic_vertices[2*dim*j+dim+1],vy);
                float wz2 = (dim > 2 ? f(basic_vertices[2*dim*j+dim+2],vz) : 0.0);
                float wt2 = (dim > 3 ? f(basic_vertices[2*dim*j+dim+3],vt) : 0.0);
                lute_attributes[elts_per_strata*i+2*edges_per_strata*k+2*j] = {{ wx1, wy1, wz1, wt1 }, {colx(wx1), colx(wy1), colx(wz1)}};
                lute_attributes[elts_per_strata*i+2*edges_per_strata*k+2*j+1] = {{ wx2, wy2, wz2, wt2 }, {colx(wx2), colx(wy2), colx(wz2)}};
            }
        }
    }
    glGenBuffers(1, &vbo_lute);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_lute);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lute_attributes), lute_attributes, GL_STATIC_DRAW);

    GLuint vs, fs;
    if ((vs = create_shader("lute.v.glsl", GL_VERTEX_SHADER))    == 0) return false;
    if ((fs = create_shader("lute.f.glsl", GL_FRAGMENT_SHADER))  == 0) return false;

    GLint link_ok = GL_FALSE;
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cerr << "glLinkProgram:";
        print_log(program);
        return false;
    }

    const char* attribute_name = "coord4d";
    attribute_coord4d = glGetAttribLocation(program, attribute_name);
    if (attribute_coord4d == -1) {
        cerr << "Could not bind attribute " << attribute_name << endl;
        return false;
    }

    attribute_name = "v_color";
    attribute_v_color = glGetAttribLocation(program, attribute_name);
    if (attribute_v_color == -1) {
        cerr << "Could not bind attribute " << attribute_name << endl;
        return false;
    }

    const char *uniform_name = "fade";
    uniform_fade = glGetUniformLocation(program, uniform_name);
    if (uniform_fade == -1) {
        cerr << "Could not bind uniform " << uniform_name << endl;
        return false;
    }
    uniform_name = "mvp";
    uniform_mvp = glGetUniformLocation(program, uniform_name);
    if (uniform_mvp == -1) {
        cerr << "Could not bind uniform " << uniform_name << endl;
        return false;
    }
    return true;
}

int frame_num = 0;
void render(SDL_Window *window) {
    glClearColor(1.0,1.0,1.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glLineWidth(2.0);
    glUseProgram(program);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_lute);
    glEnableVertexAttribArray(attribute_coord4d);
    glVertexAttribPointer(
        attribute_coord4d,
        4,
        GL_FLOAT,
        GL_FALSE,
        sizeof(struct attributes),
        0);

    glEnableVertexAttribArray(attribute_v_color);
    glVertexAttribPointer(
        attribute_v_color,
        3,
        GL_FLOAT,
        GL_FALSE,
        sizeof(struct attributes),
        (GLvoid*) offsetof(struct attributes, v_color));

    int size;
    glBindBuffer(GL_ARRAY_BUFFER, vbo_lute);
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
    glDrawArrays(GL_LINES, 0, size/sizeof(struct attributes));

/*    std::string first = std::to_string(frame_num);
    while (first.length() < 4)
        first = "0" + first;
    std::string s = "frame_" + first  + ".bmp";
    glReadPixels(0, 0, screen_width, screen_height, GL_RGB, GL_UNSIGNED_BYTE, pixels);
    FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, screen_width, screen_height, 3*screen_width, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
    FreeImage_Save(FIF_BMP, image, s.c_str(), 0);
    FreeImage_Unload(image);

    ++frame_num;
*/

    glDisableVertexAttribArray(attribute_coord4d);
    glDisableVertexAttribArray(attribute_v_color);
    SDL_GL_SwapWindow(window);
}

void free_resources() {
    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_lute);
}

float aspectaxis() {
    float outputzoom = 1.0f;
    float aspectorigin = 16.0f / 9.0f;
    int aspectconstraint = 1;
    switch (aspectconstraint) {
        case 1:
            if ((screen_width / screen_height) < aspectorigin) {
                outputzoom *= (((float)screen_width/screen_height)/aspectorigin);
            } 
            break;
        case 2:
            outputzoom *= (((float)screen_width/screen_height)/aspectorigin);
            break;
    }
    return outputzoom;
}

float recalculatefov() {
    return 2.0f * glm::atan(glm::tan(glm::radians(45.0f/2.0f)) / aspectaxis());
}

void logic() {
    float cur_time = ((float)SDL_GetTicks())/1000.0;
    float angle = SDL_GetTicks() / 1000.0 * 45;
        
    glm::vec3 axis_3(PI,1,1);
    glm::vec3 axis_z(0,0,1);
    glm::vec3 axis_y(0,1,0);
//    glm::mat4 anim =  glm::rotate(glm::mat4(1.0f), glm::radians(angle/5), axis_y);
//    glm::mat4 anim =  glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), axis_y);
    glm::mat4 anim = glm::mat4(1.0f);
    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.0));
    glm::mat4 view = glm::lookAt(glm::vec3(0.0,0.0,pow(r,cur_time/2)), glm::vec3(0.0, 0.0, -4.0), glm::vec3(0.0,1.0, 0.0));
//    glm::mat4 view = glm::lookAt(glm::vec3(0.0,0.0,1.0), glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0,1.0, 0.0));
    glm::mat4 projection = glm::perspective(recalculatefov(), 1.0f*screen_width / screen_height, 0.00000001f, 10.0f);
    glm::mat4 mvp = projection*view*model*anim;

    glUseProgram(program);
    glUniform1f(uniform_fade, 1.0f);
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
}

void mainLoop(SDL_Window *window) {
    while (true) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT)
                return;
        }
        logic();
        render(window);
    }
}

int main(int argc, char *argv[]) {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Lute",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        screen_width, screen_height,
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
    if (window == NULL) {
        cerr << "Error: can't create window: " << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 1);
    if (SDL_GL_CreateContext(window) == NULL) {
        cerr << "Error: SDL_GL_CreateContext: " << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }
    SDL_PumpEvents();
    SDL_SetWindowSize(window, screen_width, screen_height);
    glewExperimental = GL_TRUE;
    GLenum glew_status = glewInit();
    if (glew_status != GLEW_OK) {
        cerr << "Error: glewInit: " << glewGetErrorString(glew_status) << endl;
        return EXIT_FAILURE;
    }
    if (!GLEW_VERSION_2_0) {
        cerr << "Error: your graphics card does not support OpenGL 2.0" << endl;
        return EXIT_FAILURE;
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (!init_resources())
        return EXIT_FAILURE;

    mainLoop(window);

    free_resources();
    return EXIT_SUCCESS;
}

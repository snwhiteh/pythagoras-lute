attribute vec4 coord4d;
attribute vec3 v_color;
varying vec3 f_color;
uniform mat4 mvp;

float r = 0.61803398874989;
float tau = 1+r;
float c = 2+tau+tau+r;

// general stereographic projection through the point p, using l1, l2, l3 as an ortho basis

vec3 general_stereo(vec4 v, vec4 p, vec4 l1, vec4 l2, vec4 l3) {
    vec4 im = p - (v - p)/(1 - dot(v, p));
    return vec3(dot(im, l1), dot(im, l2), dot(im, l3));
}

vec3 general_ortho(vec4 v, vec4 l1, vec4 l2, vec4 l3) {
    return vec3(dot(v, l1), dot(v, l2), dot(v, l3));
}

vec3 proj(vec4 v) {
    return vec3(v.x, v.y, v.z);
//    return general_stereo(v, vec4(0,0,0,1), vec4(1,0,0,0), vec4(0,1,0,0), vec4(0,0,1,0));
//    return general_stereo(v, vec4(0,0,1,0), vec4(1,0,0,0), vec4(0,1,0,0), vec4(0,0,0,1));
//    return general_stereo(v, 1.0/sqrt((2+tau)*(2+tau)+c*c)*vec4(c, 0, 2+tau, 0), vec4(0, 0, 0, 1), vec4(0, 1, 0, 0), 1.0/sqrt((2+tau)*(2+tau)+c*c)*vec4(2+tau, 0, -c, 0) );
//    return general_ortho(v,vec4(0,1,0,0),   1.0/sqrt((2+tau)*(2+tau)+c*c)*vec4(2+tau, 0, -c, 0),vec4(0, 0, 0, 1) );
}

void main(void) {
    gl_Position = mvp*vec4(proj(coord4d), 1.0);
    f_color = v_color;
}

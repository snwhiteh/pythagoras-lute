CXXFLAGS=-g -std=c++14
LDLIBS=-lglut -lGLEW -lGL -lSDL2
all: lute
lute: shader_utils.o
clean:
	rm -f *.o lute
.PHONY: all clean

# A program to generate generalized lutes of Pythagoras

Program for visualization of generalized lutes of Pythagoras.
This code is WIP, but contains the basic elements one should need to get started.
The display is done with OpenGL 2, and works on modern Linux x64 systems at least.
It does not currently work on modern versions of Mac OSX.

The kind of lute displayed is controlled by a define flag at the top of `lute.cc`.
The possible options are `OCTAGON`, `HEXAGON`, `DODECAHEDRON`, and `DODECAPLEX`.

The function `render` has code (commented out) to export frames of the program to bitmaps. These can be used to generate videos if desired, and relies on the `FreeImage` library.

The function `logic` contains a number of animation matrices to rotate and zoom on the lute as time progresses.

Finally, the vertex shader `lute.v.glsl` contains a suite of projection functions that can be used to, for example, project the four-dimensional lute down into three dimensions.
One can also try projecting the three-dimensional dodecahedral lute to various non-orthogonal two-dimensional projections (like a stereographic one).

## Building

The project should be able to be compiled on any compatible Linux system using the provided makefile, assuming the relevant libraries and headers are present (OpenGL2, GLUT, SDL2, and FreeImage (optional)).

## Videos

See the videos folder for some example videos made with this program.